﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RDotNet;


namespace aRchaeology
{
    public class caSetting
    {
      private string rowPoints;
      private string rowLabels;
      private string rowColour;
      private string colPoints;
      private string colLabels;
      private string colColour;
      private string unlinkedData;

      public caSetting(string iRowPoints, string iRowLabels, string iRowColour, string iColPoints, string iColLabels, string iColColour, string iUnlinkedData)
      {
        rowPoints = iRowPoints;
        rowLabels = iRowLabels;
        rowColour = iRowColour;
        colPoints = iColPoints;
        colLabels = iColLabels;
        colColour = iColColour;
        unlinkedData = iUnlinkedData;
      }

      private int translateLabel(string label)
      {
        switch (label)
        {
          case "points only": return 0;
          case "labels only": return 1;
          default: return 2;
        }
      }

      public string GetOptionString()
      {
        return "\"" + rowPoints + "\", " +
               translateLabel(rowLabels).ToString() + ", " +
               "\"" + rowColour + "\", " +
               "\"" + colPoints + "\", " +
               translateLabel(colLabels).ToString() + ", " +
               "\"" + colColour + "\", " +
               "\"" + unlinkedData + "\"";
      }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        REngine engine;
        DataTable dt;

        public MainWindow()
        {
          InitializeComponent();
        }
        
        private void frmMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
          try
          {
            REngine.SetEnvironmentVariables();
            engine = REngine.GetInstance();
            engine.Initialize();

            LoadModules();
            CreateDropDowns();
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void LoadModules()
        {
          LoadModule("System.R");
          LoadModule("Ca.R");
          LoadModule("Seriation.R");
        }

        private void LoadModule(string module)
        {
          try
          {
            RunRCommand("source(\"scripts/" + module + "\")");
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void CreateDropDowns()
        {
          try
          {
            RunRCommand("x <- GetColours()");
            string[] dtColour = engine.GetSymbol("x").AsCharacter().ToArray();
            foreach (string colour in dtColour)
            {
              BrushConverter bc = new BrushConverter();
              if (bc.IsValid(colour))
              {
                RibbonGalleryItem rgiRow = new RibbonGalleryItem();
                rgiRow.Content = colour;
                rgiRow.Foreground = (Brush)bc.ConvertFrom(colour);
                rgiRow.IsSelected = (colour == "blue");
                rgcCaRowColour.Items.Add(rgiRow); 

                RibbonGalleryItem rgiCol = new RibbonGalleryItem();
                rgiCol.Content = colour;
                rgiCol.Foreground = (Brush)bc.ConvertFrom(colour);
                rgiCol.IsSelected = (colour == "red");
                rgcCaColColour.Items.Add(rgiCol);
              }
            }

            RunRCommand("x <- GetPanelValues()");
            string[] dtPanels = engine.GetSymbol("x").AsCharacter().ToArray();
            foreach (string panel in dtPanels)
            {
              RibbonGalleryItem rgiPanel = new RibbonGalleryItem();
              rgiPanel.Content = panel;
              rgiPanel.IsSelected = (panel == "blocks");
              rgcSeriationPanel.Items.Add(rgiPanel);
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        public SymbolicExpression RunRCommand(string rCommand)
        {
          StringWriter sw = new StringWriter();
          Console.SetOut(sw);
          try
          {
            return this.engine.Evaluate(rCommand);
          }
          catch (ParseException pe)
          {
            throw new Exception(sw.ToString());
          }
          catch (Exception ex)
          {
            throw ex;
          }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            RunRCommand("OpenFile()");
            LoadTable("datatable");
          }    
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            RunRCommand("SaveFile()");
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            About frmAbout = new About();
            frmAbout.ShowDialog();
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void frmMainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
              RunRCommand("CloseWithoutSaving()");
              engine.Dispose();
            }
            catch (Exception ex)
            {
              System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
        }

        private void rbDataImport_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            RunRCommand("ImportTable()");
            LoadTable("datatable");
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rbDataExport_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            if (RObjectExists("datatable"))
            {
              RunRCommand("ExportTable()");
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rgClipboardCopy_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            if (RObjectExists("datatable"))
            {
              RunRCommand("CopyTable()");
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rgClipboardPaste_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            RunRCommand("PasteTable()");
            LoadTable("datatable");
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rgMatrixPresenceAbsence_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            if (RObjectExists("datatable"))
            {
              RunRCommand("ConvertToPresenceAbsenceTable()");
              LoadTable("datatable");
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rgCaRun_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            if (RObjectExists("datatable"))
            {
              RunRCommand("settings <- GetCaSettings(" + GetCaSettingString() + ")");
              RunRCommand("reloadTable <- RunAndPlotCa(settings)");
              string[] dtExists = engine.GetSymbol("reloadTable").AsCharacter().ToArray();
              if (dtExists[0] == "TRUE")
              {
                LoadTable("datatable");
              }
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rgCaExportSvg_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            if (RObjectExists("datatable"))
            {
              RunRCommand("settings <- GetCaSettings(" + GetCaSettingString() + ")");
              RunRCommand("reloadTable <- RunCaAndSavePlotAsSvg(settings)");
              string[] dtExists = engine.GetSymbol("reloadTable").AsCharacter().ToArray();
              if (dtExists[0] == "TRUE")
              {
                LoadTable("datatable");
              }
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rbSeriationRun_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            if (RObjectExists("datatable"))
            {
              RunRCommand("settings <- GetSeriationSettings(" + GetSeriationSettingString() + ")");
              RunRCommand("reloadTable <- RunAndPlotSeriation(settings)");
              string[] dtExists = engine.GetSymbol("reloadTable").AsCharacter().ToArray();
              if (dtExists[0] == "TRUE")
              {
                LoadTable("datatable");
              }
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private void rgSeriationExportSvg_Click(object sender, RoutedEventArgs e)
        {
          try
          {
            if (RObjectExists("datatable"))
            {
              RunRCommand("settings <- GetSeriationSettings(" + GetSeriationSettingString() + ")");
              RunRCommand("reloadTable <- RunSeriationAndSavePlotAsSvg(settings)");
              string[] dtExists = engine.GetSymbol("reloadTable").AsCharacter().ToArray();
              if (dtExists[0] == "TRUE")
              {
                LoadTable("datatable");
              }
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private bool RObjectExists(string objectname)
        {
          try
          {
            RunRCommand("e <- ObjectExists(" + objectname + ")");
            string[] dtExists = engine.GetSymbol("e").AsCharacter().ToArray();
            return (dtExists[0] == "TRUE");
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
            return false;
          }
        }

        private void LoadTable(string datatable)
        {
          try
          {
            if (RObjectExists(datatable))
            {
              DataFrame df = RunRCommand(datatable).AsDataFrame();
              dt = RDataFrameToDataSet(df);
              dgTable.ItemsSource = dt.DefaultView;
            }
          }
          catch (Exception ex)
          {
            System.Windows.Forms.MessageBox.Show(ex.ToString());
          }
        }

        private static DataTable RDataFrameToDataSet(DataFrame resultsMatrix)
        {
          var dt = new DataTable();
          var columns = new DataColumn[resultsMatrix.ColumnCount + 1];

          columns[0] = new DataColumn("Units", typeof(string));
          for (int i = 0; i < resultsMatrix.ColumnCount; i++)
          {
            columns[i+1] = new DataColumn(resultsMatrix.ColumnNames[i], typeof(double));
          }

          dt.Columns.AddRange(columns);

          for (int y = 0; y < resultsMatrix.RowCount; y++)
          {
            var dr = dt.NewRow();
            dr[0] = resultsMatrix.RowNames[y];
            for (int x = 0; x < resultsMatrix.ColumnCount; x++)
            {
              try
              {
                dr[x+1] = resultsMatrix[y, x];
              }
              catch (Exception ex)
              {
                Console.WriteLine(ex.Message);
              }
            }
            dt.Rows.Add(dr);
          }

          return dt;
        }

        private string GetCaSettingString()
        {
          caSetting caOpts = new caSetting(
            rgCaRowPoints.Text,
            rgCaRowLabels.Text, 
            rgCaRowColour.Text,
            rgCaColPoints.Text,
            rgCaColLabels.Text,
            rgCaColColour.Text,
            rgCaDataUnlinked.Text);
          return caOpts.GetOptionString();
        }

        private string GetSeriationSettingString()
        {
          string setting = rgSeriationFontsize.Text + "," + 
                           "\"" + rgSeriationPanel.Text + "\"";
          return setting;
        }

    }
}
