﻿#pragma checksum "..\..\MainWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "028A877EEFA64B2ECA2692DC14141C1F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace aRchaeology {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Controls.Ribbon.RibbonWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.Ribbon RibbonWin;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rqatbSave;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonApplicationMenu ramSave;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonApplicationMenuItem ramiOpen;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonApplicationMenuItem ramiSave;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonApplicationMenuItem ramiAbout;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonApplicationMenuItem ramiExit;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonTab rtHome;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgData;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rbDataImport;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rbDataExport;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgClipboard;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rgClipboardCopy;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rgClipboardPaste;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgMatrix;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rgMatrixConvertToPresenceAbsence;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonTab rtCa;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgCa;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rgCaRun;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgCaExport;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rgCaExportSvg;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgCaRow;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgCaRowPoints;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgCaRowLabels;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgCaRowColour;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGalleryCategory rgcCaRowColour;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgCaCol;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgCaColPoints;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgCaColLabels;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgCaColColour;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGalleryCategory rgcCaColColour;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgCaData;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgCaDataUnlinked;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonTab rtSeriation;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgSeriation;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rgSeriationRun;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgSeriationExport;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonButton rgSeriationExportSvg;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGroup rgSeriationSettings;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgSeriationPanel;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGalleryCategory rgcSeriationPanel;
        
        #line default
        #line hidden
        
        
        #line 156 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonComboBox rgSeriationFontsize;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Ribbon.RibbonGalleryCategory rgcSeriationFontsize;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgTable;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/aRchaeology;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 4 "..\..\MainWindow.xaml"
            ((aRchaeology.MainWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.frmMainWindow_Loaded);
            
            #line default
            #line hidden
            
            #line 4 "..\..\MainWindow.xaml"
            ((aRchaeology.MainWindow)(target)).Closing += new System.ComponentModel.CancelEventHandler(this.frmMainWindow_Closing);
            
            #line default
            #line hidden
            return;
            case 2:
            this.RibbonWin = ((System.Windows.Controls.Ribbon.Ribbon)(target));
            return;
            case 3:
            this.rqatbSave = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 20 "..\..\MainWindow.xaml"
            this.rqatbSave.Click += new System.Windows.RoutedEventHandler(this.btnSave_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.ramSave = ((System.Windows.Controls.Ribbon.RibbonApplicationMenu)(target));
            return;
            case 5:
            this.ramiOpen = ((System.Windows.Controls.Ribbon.RibbonApplicationMenuItem)(target));
            
            #line 27 "..\..\MainWindow.xaml"
            this.ramiOpen.Click += new System.Windows.RoutedEventHandler(this.btnOpen_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ramiSave = ((System.Windows.Controls.Ribbon.RibbonApplicationMenuItem)(target));
            
            #line 28 "..\..\MainWindow.xaml"
            this.ramiSave.Click += new System.Windows.RoutedEventHandler(this.btnSave_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ramiAbout = ((System.Windows.Controls.Ribbon.RibbonApplicationMenuItem)(target));
            
            #line 29 "..\..\MainWindow.xaml"
            this.ramiAbout.Click += new System.Windows.RoutedEventHandler(this.btnAbout_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ramiExit = ((System.Windows.Controls.Ribbon.RibbonApplicationMenuItem)(target));
            
            #line 30 "..\..\MainWindow.xaml"
            this.ramiExit.Click += new System.Windows.RoutedEventHandler(this.btnExit_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.rtHome = ((System.Windows.Controls.Ribbon.RibbonTab)(target));
            return;
            case 10:
            this.rgData = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 11:
            this.rbDataImport = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 38 "..\..\MainWindow.xaml"
            this.rbDataImport.Click += new System.Windows.RoutedEventHandler(this.rbDataImport_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.rbDataExport = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 39 "..\..\MainWindow.xaml"
            this.rbDataExport.Click += new System.Windows.RoutedEventHandler(this.rbDataExport_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.rgClipboard = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 14:
            this.rgClipboardCopy = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 43 "..\..\MainWindow.xaml"
            this.rgClipboardCopy.Click += new System.Windows.RoutedEventHandler(this.rgClipboardCopy_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.rgClipboardPaste = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 44 "..\..\MainWindow.xaml"
            this.rgClipboardPaste.Click += new System.Windows.RoutedEventHandler(this.rgClipboardPaste_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.rgMatrix = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 17:
            this.rgMatrixConvertToPresenceAbsence = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 48 "..\..\MainWindow.xaml"
            this.rgMatrixConvertToPresenceAbsence.Click += new System.Windows.RoutedEventHandler(this.rgMatrixPresenceAbsence_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.rtCa = ((System.Windows.Controls.Ribbon.RibbonTab)(target));
            return;
            case 19:
            this.rgCa = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 20:
            this.rgCaRun = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 56 "..\..\MainWindow.xaml"
            this.rgCaRun.Click += new System.Windows.RoutedEventHandler(this.rgCaRun_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.rgCaExport = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 22:
            this.rgCaExportSvg = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 61 "..\..\MainWindow.xaml"
            this.rgCaExportSvg.Click += new System.Windows.RoutedEventHandler(this.rgCaExportSvg_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.rgCaRow = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 24:
            this.rgCaRowPoints = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 25:
            this.rgCaRowLabels = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 26:
            this.rgCaRowColour = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 27:
            this.rgcCaRowColour = ((System.Windows.Controls.Ribbon.RibbonGalleryCategory)(target));
            return;
            case 28:
            this.rgCaCol = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 29:
            this.rgCaColPoints = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 30:
            this.rgCaColLabels = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 31:
            this.rgCaColColour = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 32:
            this.rgcCaColColour = ((System.Windows.Controls.Ribbon.RibbonGalleryCategory)(target));
            return;
            case 33:
            this.rgCaData = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 34:
            this.rgCaDataUnlinked = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 35:
            this.rtSeriation = ((System.Windows.Controls.Ribbon.RibbonTab)(target));
            return;
            case 36:
            this.rgSeriation = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 37:
            this.rgSeriationRun = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 140 "..\..\MainWindow.xaml"
            this.rgSeriationRun.Click += new System.Windows.RoutedEventHandler(this.rbSeriationRun_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            this.rgSeriationExport = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 39:
            this.rgSeriationExportSvg = ((System.Windows.Controls.Ribbon.RibbonButton)(target));
            
            #line 145 "..\..\MainWindow.xaml"
            this.rgSeriationExportSvg.Click += new System.Windows.RoutedEventHandler(this.rgSeriationExportSvg_Click);
            
            #line default
            #line hidden
            return;
            case 40:
            this.rgSeriationSettings = ((System.Windows.Controls.Ribbon.RibbonGroup)(target));
            return;
            case 41:
            this.rgSeriationPanel = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 42:
            this.rgcSeriationPanel = ((System.Windows.Controls.Ribbon.RibbonGalleryCategory)(target));
            return;
            case 43:
            this.rgSeriationFontsize = ((System.Windows.Controls.Ribbon.RibbonComboBox)(target));
            return;
            case 44:
            this.rgcSeriationFontsize = ((System.Windows.Controls.Ribbon.RibbonGalleryCategory)(target));
            return;
            case 45:
            this.dgTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

